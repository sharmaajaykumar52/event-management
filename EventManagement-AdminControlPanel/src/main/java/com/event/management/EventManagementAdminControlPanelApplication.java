package com.event.management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

@SpringBootApplication
@EnableAdminServer
public class EventManagementAdminControlPanelApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventManagementAdminControlPanelApplication.class, args);
	}

}
