package com.event.management.Model;

import com.fasterxml.jackson.annotation.JsonAlias;

public class ContactDetailsRequest {
	@JsonAlias({ "contactName", "contact_Name", "contact_name", "name" })
	private String contactName;
	@JsonAlias({ "emailId", "email_Id", "emailId" })
	private String emailId;
	@JsonAlias({ "mobileNumber", "mobilenumber", "contactNumber" })
	private String mobileNumber;
	@JsonAlias({ "message", "mesg", "data" })
	private String message;

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ContactDetailsRequest [ " + "contactName=" + contactName + ", emailId=" + emailId + ", mobileNumber="
				+ mobileNumber + ", message=" + message + "]";
	}

	public ContactDetailsRequest(String contactName, String emailId, String mobileNumber, String message) {
		super();
		this.contactName = contactName;
		this.emailId = emailId;
		this.mobileNumber = mobileNumber;
		this.message = message;
	}

	public ContactDetailsRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

}
