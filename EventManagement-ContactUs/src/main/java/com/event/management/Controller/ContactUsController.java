package com.event.management.Controller;

import java.util.Date;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.event.management.Model.ContactDetailsRequest;

@RestController
@CrossOrigin(origins = "*")
public class ContactUsController {

	@GetMapping(value = "/ping")
	public String pingMe() {
		return "Contact Us Services are Up and running now  " + new Date().toString();
	}

	@PostMapping(value = "/addDetails", consumes = {MediaType.APPLICATION_JSON_VALUE})
	public String getContactDetail(@RequestBody ContactDetailsRequest contactDetails) {
		System.out.println("Contact Details are " + contactDetails.toString());
		return "Data recevied successfully !!!!";
	}

}
