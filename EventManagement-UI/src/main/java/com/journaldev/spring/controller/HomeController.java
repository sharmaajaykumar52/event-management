package com.journaldev.spring.controller;

import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.journaldev.spring.Utility.Helper;

@Controller
public class HomeController {

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		Helper.displayMethodExecutionName(locale, "Home");
		return "home";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showLoginPage(Locale locale, Model model) {
		Helper.displayMethodExecutionName(locale, "Login");
		return "login";
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String showRegisterationPage(Locale locale, Model model) {
		Helper.displayMethodExecutionName(locale, "Register");
		return "register";
	}
}
