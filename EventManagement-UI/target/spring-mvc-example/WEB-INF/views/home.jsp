<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%> <%@ taglib
uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <%@ page
session="false"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no"
	charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<jsp:include page="./cdnFiles.jsp" />
<title>Book your Event</title>
<style>
#paddingInDiv {
	padding-top: 10px;
}

.category-img {
	background-image: url(./Events.jpg);
	width: 70px;
	height: 60px;
	margin-left: 100px;
}

.event-mgmt-img-wa {
	background-position: -143px 3px;
}

.event-mgmt-img-birthdays {
	background-position: -75px -121px;
}

.event-mgmt-img-marriages {
	background-position: -214px -184px;
}

.event-mgmt-img-tp {
	background-position: -144px -242px;
}

.event-mgmt-img-tbe {
	background-position: -8px -242px;
}

.event-mgmt-img-conf {
	background-position: 79px -184px;
}

.card {
	display: block;
	margin-bottom: 20px;
	line-height: 1.42857143;
	background-color: #fff;
	border-radius: 2px;
	box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0
		rgba(0, 0, 0, 0.12);
	transition: box-shadow .25s;
}

.card:hover {
	box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0
		rgba(0, 0, 0, 0.19);
}

.img-card {
	width: 100%;
	height: 200px;
	border-top-left-radius: 2px;
	border-top-right-radius: 2px;
	display: block;
	overflow: hidden;
}

.img-card img {
	width: 100%;
	height: 200px;
	object-fit: cover;
	transition: all .25s ease;
}

.card-content {
	padding: 15px;
	text-align: left;
}

.card-title {
	margin-top: 0px;
	font-weight: 700;
	font-size: 1.65em;
}

.card-title a {
	color: #000;
	text-decoration: none !important;
}

.card-read-more {
	border-top: 1px solid #D4D4D4;
}

.card-read-more a {
	text-decoration: none !important;
	padding: 10px;
	font-weight: 600;
	text-transform: uppercase
}

.navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle {
	background-color: #ddd;
	margin-right: 10%;
}

body {
	background-color: rgba(225, 210, 256, 0.6);
}

.navbar-default {
	background-color: #483d8b;
	border-color: #483d8b;
}

.blockSizeDiv {
	background-color: antiquewhite;
}

.boxShadow {
	box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0
		rgba(0, 0, 0, 0.19);
}

#myNavbar>ul {
	padding-top: 1%;
}

.navbar-default .navbar-nav>li>a {
	color: whitesmoke
}

#centeredText>span {
	text-align: center;
	font-size: x-large
}

@media ( min-width : 300px) and (max-width: 640px) {
	.div-padding {
		margin-top: 18%;
	}
	#centeredText {
		margin-left: -6%;
		margin-bottom: 10%;
	}
}

/*  Medium devices (tablets, 768px and up) */
@media ( min-width : 641px) and (max-width: 768px) {
	.div-padding {
		margin-top: 10%;
	}
	#centeredText {
		margin-right: 8%;
		padding: 0%;
	}
}

/*  Large devices (desktops, 992px and up) */
@media ( min-width :769px)and (max-width: 992px) {
	.div-padding {
		margin-top: 8%;
	}
	#centeredText {
		margin-right: 8%;
		padding: 0%;
	}
}

/*    // Extra large devices (large desktops, 1200px and up) */
@media ( min-width :993px) and (max-width: 1300px) {
	.div-padding {
		margin-top: 7%;
	}
	#centeredText {
		margin-right: 8%;
		padding: 0%;
	}
}

@media ( min-width : 1301px) and (max-width:1500px) {
	.div-padding {
		margin-top: 6%;
	}
	#centeredText {
		margin-right: 8%;
		padding: 0%;
	}
}
</style>
</head>

<body>
	<jsp:include page="./navigationBar.jsp" />

	<div class="container-fluid div-padding">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
				<li data-target="#myCarousel" data-slide-to="3"></li>
			</ol>
			<div class="carousel-inner">
				<div class="item active">
					<img src="Corporate-Event-1.jpg" class="img-responsive"
						alt="Los Angeles" style="width: 100%; height: 550px;">
				</div>

				<div class="item">
					<img src="Corporate-Event-2.jpg" class="img-responsive"
						alt="Chicago" style="width: 100%; height: 550px;">
				</div>

				<div class="item">
					<img src="Corporate-Event-3.jpg" class="img-responsive"
						alt="New york" style="width: 100%; height: 550px;">
				</div>
				<div class="item">
					<img src="Corporate-Event-4.jpg" class="img-responsive"
						alt="New york" style="width: 100%; height: 550px;">
				</div>
				<!-- Left and right controls -->
				<a class="left carousel-control" href="#myCarousel"
					data-slide="prev"> <span
					class="glyphicon glyphicon-chevron-left"></span> <span
					class="sr-only">Previous</span>
				</a> <a class="right carousel-control" href="#myCarousel"
					data-slide="next"> <span
					class="glyphicon glyphicon-chevron-right"></span> <span
					class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</div>
	<!-- col-xs-12 col-md-6 col-lg-4 -->
	<!-- <div class="container-fluid row div-padding" id="events" style="margin-left:8%">        
        <div class="col-sm-12">
            <div class="col-sm-5 col-xs-12 blockSizeDiv boxShadow" id="centeredText">
                <div class="media">
                    <div class="media-left media-middle">
                        <img src="Event-1.jpg" class="media-object" style="width:80px">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><b>Corporate Events</b></h4>
                        <p> <u><i>Corporate</i> event</u> is any form of event, hospitality or social activity which is
                            organised or
                            funded
                            by a business entity</p>
                        <p>Different categories of Corporate events are <b>:-</b> </p>
                        <ul style="list-style-type:square;">
                            <li>Seminars and Conferences</li>
                            <li>Trade Shows</li>
                            <li>Team-Building Events</li>
                            <li>Appreciation Events</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-5 col-xs-12 blockSizeDiv boxShadow" id="centeredText">
                <div class="media">
                    <div class="media-left media-middle">
                        <img src="Event-1.jpg" class="media-object" style="width:80px">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><b>Private Events</b></h4>
                        <p> <u><i>Private</i> event</u> are celebrations with family members and friends. Moments of a
                            lifetime refer
                            to
                            those special events that often happen only once in a lifetime.</p>
                        <p>Different categories of Private events are <b>:-</b> </p>
                        <ul style="list-style-type:square;">
                            <li>Marriages</li>
                            <li>Birthdays</li>
                            <li>Wedding Aniversery</li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div> -->
	<div class="container-fluid row div-padding boxShadow" id="events"
		style="margin: 2%;">
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#corporateEvents">Corporarte
					Events</a></li>
			<li><a data-toggle="tab" href="#privateEvents">Private
					Events</a></li>
			<li><a data-toggle="tab" href="#openParty">Open Party</a></li>
		</ul>
		<div class="tab-content">
			<div id="corporateEvents" class="tab-pane fade in active">
				<div class="col-xs-12" style="padding: 1%">
					<div class="col-sm-4 col-xs-12 ">
						<div class="media">
							<div class="media-left media-middle">
								<div class="col-xs-12 col-sm-2.5">
									<div class="category-img event-mgmt-img-conf"></div>
								</div>
								<div class="media-body">
									<div class="col-xs-12 col-sm-10" style="text-align: center">
										<h4 class="media-heading">
											<b>Conferences</b>
										</h4>
										<p>The conference will cover a wide range of issues
											related to the teaching of psychology. It is designed for
											teachers of psychology at universities, colleges and high
											schools who are interested in enhancing their teaching
											skills, exchanging perspectives and exploring new ideas.</p>
										<p>
											For Booking & further details Please click <a href=="#"><b>Read
													More...</b></a>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-xs-12">
						<div class="media">
							<div class="media-left media-middle">
								<div class="col-xs-12 col-sm-2.5">
									<div class="category-img event-mgmt-img-tbe"></div>
								</div>
								<div class="media-body">
									<div class="col-xs-12 col-sm-10" style="text-align: center">
										<h4 class="media-heading">
											<b>Team Building Events</b>
										</h4>
										<p>The conference will cover a wide range of issues
											related to the teaching of psychology. It is designed for
											teachers of psychology at universities, colleges and high
											schools who are interested in enhancing their teaching
											skills, exchanging perspectives and exploring new ideas.</p>
										<p>
											For Booking & further details Please click <a href=="#"><b>Read
													More...</b></a>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-xs-12">
						<div class="media">
							<div class="media-left media-middle">
								<div class="col-xs-12 col-sm-2.5">
									<div class="category-img event-mgmt-img-tp"></div>
								</div>
								<div class="media-body">
									<div class="col-xs-12 col-sm-10" style="text-align: center">
										<h4 class="media-heading">
											<b>Team Party</b>
										</h4>
										<p>The conference will cover a wide range of issues
											related to the teaching of psychology. It is designed for
											teachers of psychology at universities, colleges and high
											schools who are interested in enhancing their teaching
											skills, exchanging perspectives and exploring new ideas.</p>
										<p>
											For Booking & further details Please click <a href=="#"><b>Read
													More...</b></a>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="privateEvents" class="tab-pane fade">

				<div class="col-sm-4 col-xs-12">
					<div class="media">
						<div class="media-left media-middle">
							<div class="col-xs-12 col-sm-2.5">
								<div class="category-img event-mgmt-img-marriages"></div>
							</div>
							<div class="media-body">
								<div class="col-xs-12 col-sm-10" style="text-align: center">
									<h4 class="media-heading">
										<b>Marriages</b>
									</h4>
									<p>The conference will cover a wide range of issues related
										to the teaching of psychology. It is designed for teachers of
										psychology at universities, colleges and high schools who are
										interested in enhancing their teaching skills, exchanging
										perspectives and exploring new ideas.</p>
									<p>
										For Booking & further details Please click <a href=="#"><b>Read
												More...</b></a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="col-sm-4 col-xs-12" style="padding: 1%">
					<div class="media">
						<div class="media-left media-middle">
							<div class="col-xs-12 col-sm-2.5">
								<div class="category-img event-mgmt-img-birthdays"></div>
							</div>
							<div class="media-body">
								<div class="col-xs-12 col-sm-10" style="text-align: center">
									<h4 class="media-heading">
										<b>BirthDays</b>
									</h4>
									<p>The conference will cover a wide range of issues related
										to the teaching of psychology. It is designed for teachers of
										psychology at universities, colleges and high schools who are
										interested in enhancing their teaching skills, exchanging
										perspectives and exploring new ideas.</p>
									<p>
										For Booking & further details Please click <a href="#"><b>Read
												More...</b></a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="media">
						<div class="media-left media-middle">
							<div class="col-xs-12 col-sm-2.5">
								<div class="category-img event-mgmt-img-wa"></div>
							</div>
							<div class="media-body">
								<div class="col-xs-12 col-sm-10" style="text-align: center">
									<h4 class="media-heading">
										<b>Wedding Anniversery</b>
									</h4>
									<p>The conference will cover a wide range of issues related
										to the teaching of psychology. It is designed for teachers of
										psychology at universities, colleges and high schools who are
										interested in enhancing their teaching skills, exchanging
										perspectives and exploring new ideas.</p>
									<p>
										For Booking & further details Please click <a href="#"><b>Read
												More...</b></a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>


			</div>
			<div id="openParty" class="tab-pane fade">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="media">
						<div class="media-body">
							Open houses allow your guests to come and go freely during
							designated times throughout the scheduled party time. Whether it
							is for a housewarming party, graduation party or holiday
							celebration, an open house is an easy way to spend time with all
							of your friends at the time that’s convenient for them. Here’s
							some advice on how to pull off a flawless open house party that
							will leave your guests wanting to stay all night long.
							<p>
								For Booking & further details Please click <a href="#"><b>Read
										More...</b></a>
							</p>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!--     <div class="container-fluid row div-padding" id="upComingEvents" style="background-color:green;height:400px;">
        <h1> Up-Coming-Events </h1>
    </div>  -->

	<div class="container boxShadow" id="contactUs">
		<section class="col-xs-12 col-sm-12 col-md-12"
			style="text-align: center;">
			<h2>Write Us Enquiry</h2>
		</section>
		<div class="col-xs-12 col-sm-12 col-md-12" style="padding-top: 1.5%">
			<div class="col-sm-5 col-xs-12">
				<span><b><u>Address</u></b></span> <br> BookMyEvents<br>A-36
				Sector 53 Noida Uttar Pradesh<br> <b>EmailId </b>:-
				sharma.ajaykumar52@gmail.com<br> <b>Mobile No. </b>:-+919599817440<br>
				<b>WebSite </b>:-www.bookmyevents.com<br> <br>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-7">
				<form name="contactUsModel" class="form-horizontal"
					id="contactUsModel">
					<div class="col-sm-12" id="paddingInDiv">
						<div class="row">
							<div class="col-sm-4">
								<label for="contactName" class="control-label"> <b>Name</b>
								</label>
							</div>
							<div class="col-sm-7">
								<input id="contactName" name="contactName" placeholder="Name"
									type="text" class="form-control" value=""> <span
									id="nameSpan" class="contactusspan"></span>
							</div>
						</div>
					</div>
					<div class="col-sm-12" id="paddingInDiv">
						<div class="row">
							<div class="col-sm-4">
								<label for="mobileNumber" class="control-label"> <b>Mobile
										Number</b>
								</label>
							</div>
							<div class="col-sm-7">
								<input id="mobileNumber" name="mobileNumber"
									placeholder="Mobile Number" type="text" class="form-control"
									value=""> <span id="mobileSpan" class="contactusspan"></span>
							</div>
						</div>
					</div>
					<div class="col-sm-12" id="paddingInDiv">
						<div class="row">
							<div class="col-sm-4">
								<label for="emailId" class="control-label"> <b>Email
										Id</b>
								</label>
							</div>
							<div class="col-sm-7">
								<input id="emailId" name="emailId" placeholder="Email ID"
									type="email" class="form-control" value=""> <span
									id="emailSpan" class="contactusspan"></span>
							</div>
						</div>
					</div>

					<div class="col-sm-12" id="paddingInDiv">
						<div class="row">
							<div class="col-sm-4">
								<label for="message" class="control-label"> <b>Message</b>
								</label>
							</div>
							<div class="col-sm-7">
								<textarea id="message" name="message" class="form-control"></textarea>
								<span id="messageSpan" class="contactusspan"></span>
							</div>
						</div>
					</div>
					<div class="col-sm-5" style="text-align: center; margin-top: 5%">
						<button type="button" class="btn btn-lg" id="contactUsButton"
							onClick="submitContactDetils()" style="margin-bottom: 3%">
							Submit <i class="fa fa-angle-double-right"></i>
						</button>
					</div>
				</form>

			</div>
		</div>
	</div>

	<jsp:include page="./footer.jsp" />


	 <script>

        function submitContactDetils() {
            var contactFormData = {};
            var jsonRequest="";
            $('#contactUsModel input, #contactUsModel select ,#contactUsModel textarea').each(
                function (index) {
                    var input = $(this);
                    //alert('Type: ' + input.attr('type') + 'Name: ' + input.attr('name') + 'Value: ' + input.val());                   
                    var inputName=input.attr('name');
                    var inputValue=input.val()
                    contactFormData[inputName] = inputValue;
                }
            );
                console.log("jsonRequest  -------->  "+JSON.stringify(contactFormData));

            $.ajax({
                type: "POST",
                headers: {
                    'Access-Control-Allow-Origin': '*'
                },
                url: "http://172.16.22.103:4001/contact/addDetails",
                cache: false,
                data: JSON.stringify(contactFormData),
                success: function () { console.log("data send successfully !!!!"); },
                dataType: "json",
                contentType: "application/json"
            });

        }


        $(document).ready(function () {
            // Initialize Tooltip
            $('[data-toggle="tooltip"]').tooltip();

            // Add smooth scrolling to all links in navbar + footer link
            $(".navbar a, footer a[href='#myPage']").on('click', function (event) {

                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {

                    // Prevent default anchor click behavior
                    event.preventDefault();

                    // Store hash
                    var hash = this.hash;

                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 900, function () {

                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash;
                    });
                } // End if
            });
        })
    </script>
	
	
</body>

</html>