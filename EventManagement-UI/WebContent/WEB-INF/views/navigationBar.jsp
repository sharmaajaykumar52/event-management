<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%> <%@ taglib
uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <%@ page
session="false"%>

<div class="container-fluid row">
    <nav class="navbar navbar-default navbar-fixed-top boxShadow">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#myPage">Logo</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Home</a></li>
                    <li><a href="#events">Events</a></li>
                    <li><a href="#upComingEvents">Up Coming Events</a></li>
                    <li><a href="#contactUs">Contact Us</a></li>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span>
                                Sign Up</a></li>
                        <li><a href="http://localhost:4500/spring-mvc-example/login"><span class="glyphicon glyphicon-log-in"></span>
                                Login</a></li>
                    </ul>
                </ul>
            </div>
        </div>
    </nav>
</div>