package com.ajay.mainMethod;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.ajay.Constants.Constants;
import com.ajay.FileOperation.FileOperation;
import com.ajay.GenerateBuild.ExecuteCommand;
import com.ajay.GenerateBuild.PrepareCommands;
import com.ajay.readPropertyFile.PropertyFileMapping;
import com.ajay.readPropertyFile.ReadPropertiesFile;

public class StartApplication {

	private static Logger _logger = Logger.getLogger(StartApplication.class);

	public static void main(String[] args) throws IOException, InterruptedException {
		Map<String, Object> propertiesMapObj = ReadPropertiesFile.readDataFromFile();

		if (propertiesMapObj != null) {
			_logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			_logger.info("@@@@@   File Copy Start @@@@@");
			_logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			StartApplication mainMethodObj = new StartApplication();
			mainMethodObj.startTheBuildProcess(propertiesMapObj);
			_logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			_logger.info("@@@@@   File Copy End @@@@@");
			_logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		}
	}

	public void startTheBuildProcess(Map<String, Object> propertiesMapObj) throws InterruptedException {

		String[] sourceLocation = ((String) propertiesMapObj.get(PropertyFileMapping.APPLICATION_SOURCE_LOCATION))
				.split(PropertyFileMapping.MULTIPLE_APPLICATION_PROPERTIES_FILE_DIFF);
		String destinationLocation = ((String) propertiesMapObj
				.get(PropertyFileMapping.APPLICATION_BUILD_DEPLOYMENT_LOCATION));
		String backupDestinationLocation = ((String) propertiesMapObj
				.get(PropertyFileMapping.APPLICATION_OLD_BUILD_BACKUP));
		String mavenDirectoryLocation = ((String) propertiesMapObj.get(PropertyFileMapping.MAVEN_DIRECTORY_LOCATION));
		String javaHome = ((String) propertiesMapObj.get(PropertyFileMapping.JAVA_HOME_LOCATION));
		List<String> applicablefileExtension = Arrays
				.asList(((String) propertiesMapObj.get(PropertyFileMapping.ACCEPTABLE_FILE_EXTENSION))
						.split(PropertyFileMapping.MULTIPLE_APPLICATION_PROPERTIES_FILE_DIFF));

		List<String> applicationPortDetailsList = (Arrays
				.asList(((String) propertiesMapObj.get(PropertyFileMapping.APPLICATION_BUILD_DETAILS))
						.split(PropertyFileMapping.MULTIPLE_APPLICATION_PROPERTIES_FILE_DIFF)));
		Map<String, String> applicationMap = applicationPortDetailsList.stream().collect(Collectors.toMap(
				p -> (String) p.toString().split(PropertyFileMapping.APPLICATION_PORT_PROPERTIES_FILE_DIFF)[0],
				p -> (String) p.toString().split(PropertyFileMapping.APPLICATION_PORT_PROPERTIES_FILE_DIFF)[1]));

		Vector<String> storeProcessId = new Vector();

		// Start the Maven Build
		File mavenDirectoryLocationFile = new File(mavenDirectoryLocation);
		_logger.info("Running the Maven Command to Generate Build : Start");
		for (int i = 0; i < sourceLocation.length; i++) {
			String[] commandToExecute = PrepareCommands
					.commandsToGenerateBuild(sourceLocation[i] + Constants.MAVEN_POM_XML_FILE, " install");
			ExecuteCommand executeCommand = new ExecuteCommand(commandToExecute, Constants.BUILD_GENERATE,
					mavenDirectoryLocationFile, javaHome);
			Thread threadObj = new Thread(executeCommand);
			threadObj.start();
			threadObj.join();
		}
		_logger.info("Running the Maven Command to Generate Build : End");

		_logger.info(
				"Running CMD command to find ProcessId/Kill the process according to Port where application is running	:	Start");
		// Find processId and then Stop All Services running on specific port
		for (Map.Entry<String, String> applicationMapEntrySet : applicationMap.entrySet()) {
			if (!applicationMapEntrySet.getKey().equals("0000")) {
				String[] commandToExecute = PrepareCommands
						.commandToFindProcessDetails(applicationMapEntrySet.getKey());
				ExecuteCommand executeCommand = new ExecuteCommand(commandToExecute, storeProcessId,
						mavenDirectoryLocationFile, Constants.FIND_PROCESS_DETAILS, javaHome);
				Thread threadObj = new Thread(executeCommand);
				threadObj.start();
				threadObj.join();
			}
		}
		if (storeProcessId != null && storeProcessId.size() > 0) {
			for (int i = 0; i < storeProcessId.size(); i++) {
				if (!storeProcessId.get(i).equals("") && storeProcessId.get(i).length() > 0) {
					String[] commandToExecute = PrepareCommands.commandToKillProcess(storeProcessId.get(i));
					ExecuteCommand executeCommand = new ExecuteCommand(commandToExecute, Constants.TASK_KILL, javaHome);
					Thread threadeObj = new Thread(executeCommand);
					threadeObj.start();
					threadeObj.join();
				}
			}
		}
		_logger.info(
				"Running CMD command to find ProcessId/Kill the process according to Port where application is running	:	End");

		_logger.info("Taking backup of Old Build .......");
		// Take Backup of Current Build Files
		// Delete the Old Build Files
		// Copy the new Jar files into Build Location
		FileOperation fileOperationObj = new FileOperation();
		// Take Backup
		List<File> backupLocationFiles = fileOperationObj.getListOfJarAndWarFiles(applicablefileExtension,
				Constants.OLD_BUILD_BACKUP, destinationLocation);
		_logger.info("Removing the Jar & War file from backup directory ........");
		fileOperationObj.removeOlderVersionFiles(backupDestinationLocation);
		_logger.info("Migrating the Jar & War file from backup directory ........ ");
		fileOperationObj.migrateBuildFromSourceToDestination(backupLocationFiles, backupDestinationLocation);
		// Remove
		_logger.info("Removing the Jar & War file from backup directory ........");
		fileOperationObj.removeOlderVersionFiles(destinationLocation);
		// Copy
		_logger.info("Migrating the Original Build Files from soruce location to build locatioN : Start");
		List<File> sourceLocationFiles = fileOperationObj.getListOfJarAndWarFiles(applicablefileExtension,
				Constants.BUILD_MIGRATION, sourceLocation);
		sourceLocationFiles.stream().forEach(p -> System.out.println(p.getAbsolutePath()));
		fileOperationObj.migrateBuildFromSourceToDestination(sourceLocationFiles, destinationLocation);
		_logger.info("Migrating the Original Build Files from soruce location to build locatioN : End");

		// Start the Jar File java -jar
		_logger.info("Triggering the Build for whole application : Start");
		List<File> jarFileLocation = fileOperationObj.getListOfJarAndWarFiles(applicablefileExtension,
				Constants.GET_PROCESS_NAME, destinationLocation);
		System.out.println("jarFileLocation   " + jarFileLocation.size());
		for (int i = 0; i < jarFileLocation.size(); i++) {
			if (!jarFileLocation.get(i).toString().endsWith(".war")) {
				String[] commandToExecute = PrepareCommands
						.commandToStartApplication(jarFileLocation.get(i).getAbsolutePath());
				ExecuteCommand executeCommand = new ExecuteCommand(commandToExecute, Constants.START_APPLICATION,
						javaHome);
				Thread threadObj = new Thread(executeCommand);
				threadObj.start();
			}
		}
		_logger.info("Triggering the Build for whole application : End");
		// If Any Issue Occurs then start the old jar file by copying from backup
		// location

	}

}
