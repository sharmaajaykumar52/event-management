package com.ajay.Constants;

public interface Constants {

	/* 				 URL For Testing
	 * http://172.16.22.103:4001/contact/ping
	 * 
	 * http://172.16.22.103:4001/user/ping
	 * 
	 */
	
	// Maven and CMD Commands
	public String MAVEN = "mvn -f ";
	public String CONSOLE = "cmd.exe";
	public String CONSOLE_SWITCH = "/c";
	public String COMMAND_TO_FIND_PROCESS_DETAILS = "netstat -aon | findstr ";
	public String COMMAND_TO_KILL_PROCESS_BY_ID = "Taskkill /PID " + Constants.REPLACE_PROCESS_ID + " /F";
	public String COMMAND_START_APPLICATION = "cmd /c start java -jar ";

	// Differentiator used in Application
	public String BUILD_GENERATE = "BUILD_GENERATE";
	public String GET_PROCESS_NAME = "PROCESS_NAME";
	public String BUILD_MIGRATION = "BUILD_MIGRATION";
	public String TASK_KILL = "TASK_KILL";
	public String FIND_PROCESS_DETAILS = "FIND_PROCESS_DETAILS";
	public String START_APPLICATION = "START_APPLICATION";
	public String OLD_BUILD_BACKUP = "OLD_BUILD_BACKUP";
	public String MAVEN_TARGET_LOCATION = "\\target\\";
	public String MAVEN_POM_XML_FILE = "\\pom.xml";
	public String REPLACE_PROCESS_ID = "#REPALCE-PROCESS-ID#";

	// Other File Location
	public String PROPERTY_FILE_LOCATION = "C:\\Users\\ajay.sharma\\Desktop\\EventManagement-Installer\\Properties File\\BuildDeployment.properties";
}
