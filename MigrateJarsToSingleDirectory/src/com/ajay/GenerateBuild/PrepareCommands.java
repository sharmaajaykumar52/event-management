package com.ajay.GenerateBuild;

import org.apache.log4j.Logger;

import com.ajay.Constants.Constants;

public class PrepareCommands {

	private static Logger _logger = Logger.getLogger(PrepareCommands.class);

	public static String[] commandsToGenerateBuild(String pathToPomFile, String goal) {
		String[] commandToExecute = { Constants.CONSOLE, Constants.CONSOLE_SWITCH,
				Constants.MAVEN + pathToPomFile + goal };
		_logger.info("Command To Generate Build : - " + commandToExecute[0] + " " + commandToExecute[1] + " "
				+ commandToExecute[2]);
		return commandToExecute;
	}

	public static String[] commandToKillProcess(String processId) {
		String command = Constants.COMMAND_TO_KILL_PROCESS_BY_ID.replace(Constants.REPLACE_PROCESS_ID, processId);
		String[] commandToKillProcess = { Constants.CONSOLE, Constants.CONSOLE_SWITCH, command };
		_logger.info("Command To Kill Process : - " + commandToKillProcess[0] + " " + commandToKillProcess[1] + " "
				+ commandToKillProcess[2]);
		return commandToKillProcess;
	}

	public static String[] commandToFindProcessDetails(String port) {
		String[] commandToExecute = { Constants.CONSOLE, Constants.CONSOLE_SWITCH,
				Constants.COMMAND_TO_FIND_PROCESS_DETAILS, port };
		_logger.info("Command To Find Process Details : - " + commandToExecute[0] + " " + commandToExecute[1] + " "
				+ commandToExecute[2] + " " + commandToExecute[3]);
		return commandToExecute;
	}

	public static String[] commandToStartApplication(String jarFileLocation) {
		String[] commandToExecute = { Constants.CONSOLE, Constants.CONSOLE_SWITCH,
				Constants.COMMAND_START_APPLICATION + jarFileLocation };
		_logger.info("Command To Start Application   " + commandToExecute[0] + "  " + commandToExecute[1] + "  "
				+ commandToExecute[2]);
		return commandToExecute;
	}

}
