package com.ajay.GenerateBuild;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.ajay.Constants.Constants;

public class ExecuteCommand implements Runnable {

	private static Logger _logger = Logger.getLogger(ExecuteCommand.class);

	private String[] commandToExecute;
	private Vector<String> storeProcessId = null;
	private File mavenDirectoryLocationFile;
	private String typeOfCommand;
	private String javaHome;

	public ExecuteCommand(String[] commandToExecute, String typeOfCommand, File mavenDirectoryLocationFile,
			String javaHome) {
		super();
		this.commandToExecute = commandToExecute;
		this.typeOfCommand = typeOfCommand;
		this.mavenDirectoryLocationFile = mavenDirectoryLocationFile;
		this.javaHome = javaHome;
	}

	public ExecuteCommand(String[] commandToExecute, Vector<String> storeProcessId, File mavenDirectoryLocationFile,
			String typeOfCommand, String javaHome) {
		super();
		this.commandToExecute = commandToExecute;
		this.storeProcessId = storeProcessId;
		this.mavenDirectoryLocationFile = mavenDirectoryLocationFile;
		this.typeOfCommand = typeOfCommand;
		this.javaHome = javaHome;
	}

	public ExecuteCommand(String[] commandToExecute, String typeOfCommand, String javaHome) {
		this.commandToExecute = commandToExecute;
		this.typeOfCommand = typeOfCommand;
		this.javaHome = javaHome;
	}

	@Override
	public void run() {

		ProcessBuilder processBuilder = new ProcessBuilder();
		Map<String, String> env = processBuilder.environment();
		env.put("JAVA_HOME", this.javaHome);
		env.put("Path", this.javaHome + "\\;" + env.get("Path") + ";" + this.javaHome);
		if (this.typeOfCommand.equals(Constants.BUILD_GENERATE)
				|| this.typeOfCommand.equals(Constants.START_APPLICATION)) {
			executeMavenCommands(processBuilder, "Build Generation");
		}
		if (this.typeOfCommand.equals(Constants.FIND_PROCESS_DETAILS)) {
			executeNonMavenCommands(processBuilder, "Finding the Process Details ");
		}
		if (this.typeOfCommand.equals(Constants.TASK_KILL)) {
			executeMavenCommands(processBuilder, "Task Kill");
		}

	}

	public void executeMavenCommands(ProcessBuilder processBuilder, String consoleMessage) {
		if (this.mavenDirectoryLocationFile != null) {
			processBuilder.directory(this.mavenDirectoryLocationFile);
		}
		Process p;
		try {
			_logger.info(consoleMessage + " start ...................");
			executeCommand(processBuilder, this.commandToExecute);
			p = processBuilder.start();
			BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while (true) {
				line = r.readLine();
				if (line == null) {
					break;
				}
				_logger.info(line);
			}
			_logger.info(consoleMessage + " Ends ...................");
		} catch (IOException e) {
			_logger.error("Error Occured While Executing command " + e.getMessage());
			Arrays.asList(this.commandToExecute).forEach(q -> _logger.error(q));
			e.printStackTrace();
		}
	}

	public void executeNonMavenCommands(ProcessBuilder processBuilder, String consoleMessage) {
		Process p;
		try {
			_logger.info(consoleMessage + " start ...................");
			executeCommand(processBuilder, this.commandToExecute);
			p = processBuilder.start();
			BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while (true) {
				line = r.readLine();
				if (line == null) {
					break;
				}
				if (line.contains(" ") && line.length() >= 38) {
					String portNumber = line.split(" ")[38];
					this.storeProcessId.add(portNumber);
				}
				_logger.info(line);
			}
			System.out.println(consoleMessage + " Ends ...................");
		} catch (IOException e) {
			_logger.error("Error Occured While Executing command " + e.getMessage());
			Arrays.asList(this.commandToExecute).forEach(q -> _logger.error(q));
		}
	}

	public void executeCommand(ProcessBuilder builder, String[] command) {
		builder.redirectErrorStream(true);
		builder.command(commandToExecute);

	}

	public boolean typeOfCommand(String type) {
		return type.equals(Constants.GET_PROCESS_NAME) ? true : false;
	}

}
