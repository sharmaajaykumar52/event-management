package com.ajay.readPropertyFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.ajay.Constants.Constants;
import com.ajay.FileOperation.FileOperation;

public class ReadPropertiesFile {

	private static Logger _logger = Logger.getLogger(FileOperation.class);

	public static Map<String, Object> readDataFromFile() {
		_logger.info("Reading required data from properties File : Start");
		Map<String, Object> propertiesMapObj = new HashMap<>();
		String propertyFileLocation = Constants.PROPERTY_FILE_LOCATION;
		try (InputStream input = new FileInputStream(propertyFileLocation)) {
			Properties propertiesObj = new Properties();
			propertiesObj.load(input);
			propertiesMapObj.put(PropertyFileMapping.MAVEN_DIRECTORY_LOCATION,
					propertiesObj.get(PropertyFileMapping.MAVEN_DIRECTORY_LOCATION));
			propertiesMapObj.put(PropertyFileMapping.JAVA_HOME_LOCATION,
					propertiesObj.get(PropertyFileMapping.JAVA_HOME_LOCATION));
			propertiesMapObj.put(PropertyFileMapping.ACCEPTABLE_FILE_EXTENSION,
					propertiesObj.get(PropertyFileMapping.ACCEPTABLE_FILE_EXTENSION));
			propertiesMapObj.put(PropertyFileMapping.APPLICATION_SOURCE_LOCATION,
					propertiesObj.get(PropertyFileMapping.APPLICATION_SOURCE_LOCATION));
			propertiesMapObj.put(PropertyFileMapping.APPLICATION_BUILD_DEPLOYMENT_LOCATION,
					propertiesObj.get(PropertyFileMapping.APPLICATION_BUILD_DEPLOYMENT_LOCATION));
			propertiesMapObj.put(PropertyFileMapping.APPLICATION_OLD_BUILD_BACKUP,
					propertiesObj.get(PropertyFileMapping.APPLICATION_OLD_BUILD_BACKUP));
			propertiesMapObj.put(PropertyFileMapping.APPLICATION_BUILD_DETAILS,
					propertiesObj.get(PropertyFileMapping.APPLICATION_BUILD_DETAILS));
			_logger.info("Reading required data from properties File : End");
			return propertiesMapObj;
		} catch (IOException io) {
			_logger.info("Exception Occured while reading required data from properties file " + io.getMessage());
		}
		return null;
	}	
	
	public static void main(String[] args) {
		
		Map<String,Object> propertiesMapObj = readDataFromFile();
			
	}
	
	
}
