package com.ajay.FileOperation;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.ajay.Constants.Constants;

public final class FileOperation {

	private static Logger _logger = Logger.getLogger(FileOperation.class);

	private static final void copyZipFile(String source, String destination) {
		try {
			Path sourcePath = Paths.get(source);
			Path destinationPath = Paths.get(destination + sourcePath.getFileName());
			Files.copy(sourcePath, destinationPath);
			_logger.info("Files copied Successfully from Source " + sourcePath + " to destination " + destinationPath);
		} catch (IOException e) {
			_logger.error("Exception occured while copying files from source " + source + " to destination "
					+ destination + " error description is ");
			e.printStackTrace();
		}
	}

	public final void removeOlderVersionFiles(String destinationLocation) {
		File whetherFileExist = new File(destinationLocation);
		File[] file = whetherFileExist.listFiles();
		if (file != null && file.length >= 1) {
			Arrays.asList(file).parallelStream().forEach(p -> p.delete());
		} else {
			_logger.info("No Older version files present");
		}
	}

	private static List<File> getSelectedFileFromSource(String commandType, String sourceLocation,List<String> applicablefileExtension) {
		String location = "";
		if (commandType.equals(Constants.GET_PROCESS_NAME) || commandType.equals(Constants.OLD_BUILD_BACKUP)) {
			location = sourceLocation;
		} else if (commandType.equals(Constants.BUILD_MIGRATION)) {
			location = sourceLocation + Constants.MAVEN_TARGET_LOCATION;
		}
		File sourceLocationFile = new File(location);
		List<File> sourceLocationFileList = Arrays.asList(sourceLocationFile.listFiles()).stream().filter(
				p -> p.getAbsoluteFile().toString().endsWith(".jar") || p.getAbsoluteFile().toString().endsWith(".war"))
				.collect(Collectors.toList());
		
		return sourceLocationFileList;
	}

	public List<File> getListOfJarAndWarFiles(List<String> applicablefileExtension,String commandType, String... location) {
		List<File> fileToBeDeployed = new ArrayList<File>();
		Arrays.asList(location).parallelStream().forEach(p -> {
			fileToBeDeployed.addAll(getSelectedFileFromSource(commandType, p,applicablefileExtension));
		});
		return fileToBeDeployed;
	}

	public void migrateBuildFromSourceToDestination(List<File> sourceLocationFileList, String destinationLocation) {
		sourceLocationFileList.stream().forEach(fileObj -> {
			copyZipFile(fileObj.getAbsolutePath(), destinationLocation);
		});
	}
}
