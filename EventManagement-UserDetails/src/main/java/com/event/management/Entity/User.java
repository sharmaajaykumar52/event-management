package com.event.management.Entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*@Entity
@Table(name = "User")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor*/
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 578379643709754190L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "UserId")
	private Long userId;
	@Column(name = "EmailId")
	private String emailId;
	@Column(name = "password")
	private String password;
	@Column(name = "lastOtpSent")
	private Date lastOTPSent;
	@Column(name = "OneTimePassword")
	private String oneTimePassword;
	@Column(name = "IsUserActive")
	private String isUserActive;
	@Column(name = "creationDate")
	private Date creationDate;
	@Column(name = "updateDate")
	private Date updateDate;

}
