package com.event.management.Entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

/*@Entity
@Table(name = "Privilege")*/
public class Privilege {

	private Long privilegeId;
	private String privilegeName;
	private String privilegeDescription;
	private Date creationDate;
	private Date lastUpdateDate;
	private String isPrivilegeActive;

}
