package com.event.management.Entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

/*@Entity
@Table(name = "DecorationRequest")*/
public class DecorationRequest {

	private Long decorationId;
	private String deocrationType;
	private String decorationDescription;
	private String decorationComments;
	private String approximateAreaToBeCovered;
	private String isDecorationOpted;
	private String percentageDecorationStatus;
	private String isDecorationCompelted;
	private Date decorationStartDate;
	private Date decorationEndDate;
	private Date lastUpdateDate;
	
	
}
