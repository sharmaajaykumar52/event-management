package com.event.management.Entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

/*@Entity
@Table(name = "Transaction")*/
public class PaymentGatewayRequestLog {

	private Long transactionId;
	private Date transactionDate;
	private String transactionStatus;
	private Date lastUpdateDate;
	private String completeRequest;

}
