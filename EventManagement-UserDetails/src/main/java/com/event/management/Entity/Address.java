package com.event.management.Entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Setter;

import lombok.Getter;

/*@Entity
@Table(name = "address")
@Getter
@Setter*/
public class Address implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4795577071826866758L;
	private int addressId;
	private String houseNumber;
	private String locality;
	private String state;
	private String city;
	private String country;
	private String pincode;

	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Address(int addressId, String houseNumber, String locality, String state, String city, String country,
			String pincode) {
		super();
		this.addressId = addressId;
		this.houseNumber = houseNumber;
		this.locality = locality;
		this.state = state;
		this.city = city;
		this.country = country;
		this.pincode = pincode;
	}

	@Override
	public String toString() {
		return "Address [addressId=" + addressId + ", houseNumber=" + houseNumber + ", locality=" + locality
				+ ", state=" + state + ", city=" + city + ", country=" + country + ", pincode=" + pincode + "]";
	}

}
