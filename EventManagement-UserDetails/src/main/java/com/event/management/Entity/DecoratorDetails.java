package com.event.management.Entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

/*@Entity
@Table(name = "Decorator")*/
public class DecoratorDetails {

	private Long decoratorId;
	private String deocratorName;
	private String decoratorDescription;
	private String isDecorationRequestAccepted;
	private String isDecorationTaskCompleted;
	private String decoratorRegisterationNumber;
	private String isDecoratorActive;
	private Date creationDate;
	private Date ladstUpdateDate;
	private ContactDetails decoratorContactDetails;
	private Long minimumBudget;
	private Long maximumBudget;

}
