package com.event.management.Model;

import com.fasterxml.jackson.annotation.JsonAlias;

public class UserRequest {

	@JsonAlias({ "emailId", "email_Id", "emailID", "email_ID", "username", "userName", "user_name", "user_Name" })
	private String userName;
	@JsonAlias({ "password", "pword" })
	private String password;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserRequest(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}

	public UserRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "UserRequest [userName=" + userName + ", password=" + password + "]";
	}

}
