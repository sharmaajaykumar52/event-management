package com.event.management.Controller;

import java.util.Date;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.event.management.Model.UserRequest;

@RestController
@CrossOrigin(origins = "*")
public class UserController {

	@GetMapping(value = "/ping")
	public String pingMe() {
		return "User Controller Services are Up and running now  " + new Date().toString();
	}

	// to add details
	@GetMapping(value = "/addDetails", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String addUserDetails(@RequestBody String organizerDetails) {
		System.out.println("userRequest   " + organizerDetails);
		return "AddDetails Request Logged in Successfully !!!";
	}

	// to login a user
	@PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String verifyUserDetails(@RequestBody UserRequest userRequest) {
		System.out.println("userRequest   " + userRequest.toString());
		return "Request Logged in Successfully !!!";
	}
}
