package com.event.management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventManagementUserDetailsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventManagementUserDetailsApplication.class, args);
	}

}
